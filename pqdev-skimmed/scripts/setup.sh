#!/usr/bin/env bash

echo "~## Update & Upgrading ##~"
apt update -qq && apt upgrade -qqy

echo "~## Installing base packages ##~"
apt install -qqy gnupg2 curl zip unzip wget python3 python3-pip software-properties-common

echo "~## Setup nodejs ##~"
curl -sL https://deb.nodesource.com/setup_$NODE_VERSION.x | bash -

echo "~## Installing yarn ##~"
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list

echo "~## Setup PHP Repo ##~"
add-apt-repository ppa:ondrej/php

echo "~## Update ##~"
apt update -qq

echo "~## Install remaining packages ##~"
apt install -qqy php8.1-cli php8.1-curl php8.1-zip php8.1-mbstring nodejs yarn

echo "~## Install composer ##~"
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

echo "~## Install gulp ##~"
npm install --global gulp-cli

echo "~## Install awscli ##~"
pip3 install awscli

echo "~## Clean up ##~"
apt autoremove -qq
apt clean -qq
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
